import { fabric } from "fabric"

export class ImageObject {
  object: fabric.Rect
  constructor(img: HTMLImageElement) {
    this.object = new fabric.Rect({
      width: img.width,
      height: img.height,
    })
    this.object.fill = new fabric.Pattern({
      source: img,
      repeat: 'no-repeat'
    });
    this.object.on('mouseover', () => {
      this.object.strokeWidth = 5
      this.object.stroke = '#000'
      this.object.canvas?.renderAll()
    })
    this.object.on('mouseout', () => {
      this.object.strokeWidth = 0
      this.object.canvas?.renderAll()
    })
  }

  addEvents() {
  }
}